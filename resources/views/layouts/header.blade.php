<div class="row">
    <div class="col-2"><a href="{{ route('user.index') }}" class="btn btn-primary">Danh sách người dùng</a></div>
    @can('admin')
        <div class="col-2"><a href="{{ route('user.create') }}" class="btn btn-primary">Thêm mới người dùng</a></div>
    @endcan
    <div class="col-6"></div>
    <div class="col-2">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Helper::toUpperCase(Auth::user()->name) }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
    </div>
</div>
