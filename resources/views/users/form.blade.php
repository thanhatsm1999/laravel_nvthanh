<style>
    span {
        color: red;
    }
</style>
<script>
    $(document).ready(function() {
        //jqueryemail
        @if ($errors->first('mai_address'))
            $("#ipemail").css("border", "1px solid red");
            $("#lbemail").css("color", "red");
        @endif
        //jqueryusername
        @if ($errors->first('name'))
            $("#ipusername").css("border", "1px solid red");
            $("#lbusername").css("color", "red");
        @endif
        //jqueryaddress
        @if ($errors->first('address'))
            $("#ipaddress").css("border", "1px solid red");
            $("#lbaddress").css("color", "red");
        @endif
        //jqueryphone
        @if ($errors->first('phone'))
            $("#ipphone").css("border", "1px solid red");
            $("#lbphone").css("color", "red");
        @endif
        //jquerypassword
        @if ($errors->first('password'))
            $("#ippassword").css("border", "1px solid red");
            $("#lbpassword").css("color", "red");
        @endif
        //jqueryconfirm
        @if ($errors->first('confirm_password'))
            $("#ipconfirm").css("border", "1px solid red");
            $("#lbconfirm").css("color", "red");
        @endif
    });
</script>
<div class="row">
    <div class="col">
        <label id="lbusername">Username:</label>
        <input type="text" name="name" id="ipusername" class="form-control" value="@if(isset($users->name)) {{ $users->name }} @else {{ old('name') }} @endif">
        <span>
        @if ($errors->first('name'))
            {{ $errors->first('name') }}
        @endif
        </span>
    </div>
    <div class="col">
        <label id="lbemail">Email</label>
        <input type="text" name="mai_address" id="ipemail" class="form-control" value="@if(isset($users->mai_address)) {{ $users->mai_address }} @else {{ old('mai_address') }} @endif">
        <span>
        @if ($errors->first('mai_address'))
            {{ $errors->first('mai_address') }}
        @endif
        </span>
    </div>
</div>
<div class="row">
    <div class="col">
        <label id="lbpassword">Password</label>
        <input type="password" name="password" id="ippassword" class="form-control">
        <span>
        @if ($errors->first('password'))
            {{ $errors->first('password') }}
        @endif
        </span>
    </div>
    <div class="col">
        <label id="lbconfirm">Confirm Password</label>
        <input type="password" name="confirm_password" id="ipconfirm" class="form-control">
        <span>
        @if ($errors->first('confirm_password'))
            {{ $errors->first('confirm_password') }}
        @endif
        </span>
    </div>
</div>
<div class="row">
    <div class="col">
        <label id="lbaddress">Address</label>
        <input type="text" name="address" id="ipaddress" class="form-control" value="@if(isset($users->address)) {{ $users->address }} @else {{ old('address') }} @endif">
        <span>
        @if ($errors->first('address'))
            {{ $errors->first('address') }}
        @endif
        </span>
    </div>
    <div class="col">
        <label id="lbphone">Phone</label>
        <input type="text" name="phone" id="ipphone" class="form-control" value="@if(isset($users->phone)) {{ $users->phone }} @else {{ old('phone') }} @endif">
        <span>
        @if ($errors->first('phone'))
            {{ $errors->first('phone') }}
        @endif
        </span>
    </div>
</div>
<div class="row">
    <div class="col">
        <label for="">Vai trò</label>
        <select name="role" id="" class="form-control">
            @foreach (User::role as $key => $value)
                <option value=" {{ $key }} "> {{ $value }} </option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="col-6 mt-3">
        <input type="submit" value="@if(isset($users->id)) Update @else Add @endif" class="btn btn-primary">
    </div>
</div>
