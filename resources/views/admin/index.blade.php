@extends('layouts.default')
@section('title')
    List users
@endsection
@section('content')
@include('flash::message')
<form action="{{ route('user.index') }}">
    <div class="row">
        <div class="col-4">
            <label for="">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="col-4">
            <label for="">Email</label>
            <input type="text" class="form-control" name="mai_address">
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <label for="">Address</label>
            <input type="text" class="form-control" name="address">
        </div>
        <div class="col-4">
            <label for="">Phone</label>
            <input type="text" class="form-control" name="phone">
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <input type="submit" value="Search" class="btn btn-primary">
        </div>
    </div>
</form>
@if (Session('error'))
   <h1>{{Session('error')}}</h1> 
@endif
<br>
<table class="table table-hover">
    <tr>
        <th>STT</th>
        <th>Name</th>
        <th>Email</th>
        <th>SDT</th>
        <th>Address</th>
        <th>Role</th>
        <th>Create At</th>
        <th>Action</th>
    </tr>
    @foreach ($users as $key => $us)
    <tr>
        <td>{{ $users->perPage() * ($users->currentPage() - 1) + 1 + $key }}</td>
        <td>{{ Helper::toUpperCase($us->name) }}</td>
        <td>{{ $us->mai_address }}</td>
        <td>{{ $us->phone }}</td>
        <td>{{ $us->address }}</td>
        <td>{{ User::role[$us->role] }}</td>
        <td>{{ $us->created_at }}</td>
        @can('admin')
        <td><a href="{{ route('user.update', $us->id) }}">Update</a></td>
        @endcan
    </tr>
    @endforeach
    <tr>
        <td colspan="5">{{ $users->links() }}</td>
    </tr>
</table>
@endsection
