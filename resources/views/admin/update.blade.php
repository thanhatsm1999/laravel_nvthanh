@extends('layouts.default')
@section('title')
    update User
@endsection
@section('content')
    <div class="container">
        <h1>update User</h1>
        <form method="POST" action="{{ route('user.update', $users->id) }}" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            @include('users.form')
        </form>
    </div>
@endsection
