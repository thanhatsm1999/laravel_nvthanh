@extends('layouts.default')
@section('title')
    Add users
@endsection
@section('content')
    <div class="container">
        <h1>Add a new User</h1>
        <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
            @csrf
            @include('users.form')
        </form>
    </div>
@endsection
