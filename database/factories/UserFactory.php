<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'mai_address' => $faker->unique()->safeEmail,
        'name' => $faker->name,
        'password'=> Hash::make('thanh1999'),
        'phone' => $faker->e164PhoneNumber,
        'address' => $faker->address,
        'role' => rand(User::admin, User::staff),
    ];
});
