<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FacadesHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Helper';
    }
}
