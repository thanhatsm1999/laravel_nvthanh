<?php

namespace App\model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    protected $table = "users";
    use SoftDeletes;
    protected $fillable = [
        'name',
        'mai_address',
        'address',
        'password',
        'phone',
        'role',
    ];
    protected $perPage = 20 ;
    protected $dates = ['deleted_at'];
    /**
     * add new user with user
     * param $request
     * return boolean
     */
    public function addUsers($request) 
    {
        return User::create([
            'name' => $request['name'],
            'mai_address' => $request['mai_address'],
            'password' => Hash::make($request['password']),
            'address' => $request['address'],
            'phone' => $request['phone'],
            'role' => $request['role'],
        ]);
    }
    /**
     * get all user and search with paginate
     * return array user
     */
    public function getAll($request)
    {
        $users = User::query();
        if (isset($request['name'])) {
            $users->where('name', 'like', '%'. $request['name'] . '%');
        }
        if (isset($request['mai_address'])) {
            $users->where('mai_address', 'like', '%' . $request['mai_address'] . '%');
        }
        if (isset($request['address'])) {
            $users->where('address', 'like', '%' . $request['address'] . '%');
        }
        if (isset($request['phone'])) {
            $users->where('phone', '=', $request['phone']);
        }
        return $users->paginate();
    }
    /**
     * find user with id
     * param $id
     * return user
     */
    public function show($id)
    {
        return User::find($id);
    }
    /**
     * update user 
     * parm $request, $id
     * return boolean
     */
    public function updateUser($request, $id)
    {
        $user = User::find($id);
        if(empty($request['password'])) {
            $request['password'] = Hash::make($user->password);
        }
        return $user->update($request);
    }
    /**
     * Define role
     */
    const admin = 1;
    const staff = 2;
    const role = [
        '1' => 'Quản trị viên', 
        '2' => 'Nhân viên',
    ];
}
