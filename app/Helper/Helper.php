<?php

namespace   App\Helper;

class Helper {
    public static function toUpperCase($value) 
    {
        return mb_strtoupper($value);
    }
}
