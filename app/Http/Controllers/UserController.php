<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Http\Requests\UserRequest;
use App\Jobs\JobSendEmail;
use Mail;
class UserController extends Controller
{
    private $user;
     /**
      * initialization Dependency Injection model user
      * @param  $user
      * return $user
      */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->middleware('admin')->only('create', 'show');
    }
    /**
     * move to add user
     * return view add
     */
    public function create()
    {
        return view('admin.add');
    }
    /**
     * validate input and save new user
     * param UserRequest $request 
     * return boolean
     */
    public function store(UserRequest $request)
    {
        
        $this->user->addUsers($request->all());
        JobSendEmail::dispatch($request->all());
        flash('Thêm mới người dùng thành công');
        return redirect()->route('user.index');
    }
    /**
     * get list user and search 
     * return list user
     */
    public function index(Request $request)
    {
        $users = $this->user->getAll($request->all());
        if (empty($users['0'])) {
            return redirect()->route('user.index')->with('error', 'Không tìm thấy người dùng');
        }
        return view('admin.index', compact('users'));
    }
    /**
     * get form update user
     * param $id
     * return view update
     */
    public function show($id)
    {
        $users = $this->user->show($id);
        return view('admin.update', compact('users'));
    }
    /**
     * update user
     * param $id, $request, UserRequest
     * return view index
     */
    public function update(UserRequest $request, $id)
    {
        $this->user->updateUser($request->all(), $id);
        flash('Cập nhật người dùng thành công');
        return redirect()->route('user.index');
    }
}
