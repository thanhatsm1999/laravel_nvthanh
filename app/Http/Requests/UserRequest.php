<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->getmethod() == 'PUT') {
            $id = $this->route('user');
            return [
                'name' => 'required|max:255',
                'mai_address' => 'required|email|unique:users,mai_address,'.$id,
                'password' => 'max:255',
                'confirm_password' => 'same:password',
                'address' => 'nullable|max:255',
                'phone' => 'nullable|Numeric|digits_between:1,15',
            ];
        } 
        return [
            'name' => 'required|max:255',
            'mai_address' => 'required|email|unique:users,mai_address',
            'password' => 'required|max:255',
            'confirm_password' => 'required|same:password',
            'address' => 'nullable|max:255',
            'phone' => 'nullable|Numeric|digits_between:1,15',
            ];
    }
}
